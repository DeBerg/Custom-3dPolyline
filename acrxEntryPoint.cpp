﻿// (C) Copyright 2002-2012 by Autodesk, Inc. 
//
// Permission to use, copy, modify, and distribute this software in
// object code form for any purpose and without fee is hereby granted, 
// provided that the above copyright notice appears in all copies and 
// that both that copyright notice and the limited warranty and
// restricted rights notice below appear in all supporting 
// documentation.
//
// AUTODESK PROVIDES THIS PROGRAM "AS IS" AND WITH ALL FAULTS. 
// AUTODESK SPECIFICALLY DISCLAIMS ANY IMPLIED WARRANTY OF
// MERCHANTABILITY OR FITNESS FOR A PARTICULAR USE.  AUTODESK, INC. 
// DOES NOT WARRANT THAT THE OPERATION OF THE PROGRAM WILL BE
// UNINTERRUPTED OR ERROR FREE.
//
// Use, duplication, or disclosure by the U.S. Government is subject to 
// restrictions set forth in FAR 52.227-19 (Commercial Computer
// Software - Restricted Rights) and DFAR 252.227-7013(c)(1)(ii)
// (Rights in Technical Data and Computer Software), as applicable.
//

//-----------------------------------------------------------------------------
//----- acrxEntryPoint.cpp
//-----------------------------------------------------------------------------
#include "StdAfx.h"
#include "resource.h"
#include "tchar.h"
#include "B3DPoly\AsdkB3DVertex.h"
#include "B3DPoly\AsdkB3DPolyline.h"

#pragma comment (lib ,"AsdkB3DPoly.lib") 

//-----------------------------------------------------------------------------
#define szRDS _RXST("Asdk")

//-----------------------------------------------------------------------------
//----- ObjectARX EntryPoint
class CKPipeLineApp : public AcRxArxApp {

public:
	CKPipeLineApp () : AcRxArxApp () {}

	virtual AcRx::AppRetCode On_kInitAppMsg (void *pkt) {
		// TODO: Load dependencies here

		// You *must* call On_kInitAppMsg here
		AcRx::AppRetCode retCode =AcRxArxApp::On_kInitAppMsg (pkt) ;
		
		// TODO: Add your initialization code here

		return (retCode) ;
	}

	virtual AcRx::AppRetCode On_kUnloadAppMsg (void *pkt) {
		// TODO: Add your code here

		// You *must* call On_kUnloadAppMsg here
		AcRx::AppRetCode retCode =AcRxArxApp::On_kUnloadAppMsg (pkt) ;

		// TODO: Unload dependencies here

		return (retCode) ;
	}

	virtual void RegisterServerComponents () {
	}
	
	// The ACED_ARXCOMMAND_ENTRY_AUTO macro can be applied to any static member 
	// function of the CKPipeLineApp class.
	// The function should take no arguments and return nothing.
	//
	// NOTE: ACED_ARXCOMMAND_ENTRY_AUTO has overloads where you can provide resourceid and
	// have arguments to define context and command mechanism.
	
	// ACED_ARXCOMMAND_ENTRY_AUTO(classname, group, globCmd, locCmd, cmdFlags, UIContext)
	// ACED_ARXCOMMAND_ENTRYBYID_AUTO(classname, group, globCmd, locCmdId, cmdFlags, UIContext)
	// only differs that it creates a localized name using a string in the resource file
	//   locCmdId - resource ID for localized command

	// Modal Command with localized name
	// ACED_ARXCOMMAND_ENTRY_AUTO(CKPipeLineApp, AsdkMyGroup, MyCommand, MyCommandLocal, ACRX_CMD_MODAL)
	static void AsdkMyGroupMyCommand () {
		// Put your command code here

	}


	// ----- AsdkKPipeLine._B3DPOLY command (do not rename)
	static void AsdkKPipeLine_B3DPOLY(void)
	{
		// Add your code for command AsdkStep06._CREATEEMPLOYEE here
		// Get user input for employee
		//int id, cubeNumber;
		//---------------
		//TCHAR strFirstName[133];
		///*TCHAR strLastName[133];
		//AcGePoint3d pt;
		//if (acedGetInt(_T("Enter employee ID: "), &id) != RTNORM
		//	|| acedGetInt(_T("Enter cube number: "), &cubeNumber) != RTNORM
		//	|| acedGetString(0, _T("Enter employee first name: "), strFirstName) != RTNORM
		//	|| acedGetString(0, _T("Enter employee last name: "), strLastName) != RTNORM
		//	|| acedGetPoint(NULL, _T("Employee position: "), asDblArray(pt)) != RTNORM
		//	) {
		//	return;*/
		//if (acedGetString(0, _T("Enter a string: "), strFirstName) != RTNORM) {
		//	return;
		//}
		//AsdkB3DPolyline *pB3dp = new AsdkB3DPolyline;
		//acutPrintf(_T("Input string is: "));
		//acutPrintf(strFirstName);
		//acutPrintf(_T("\n"));
		//pB3dp->close();
		//-----------------
		//}

		//// Get a pointer to the current drawing
		//// and get the drawing's block table. Open it for read.
		//AcDbBlockTable *pBlockTable;
		//if (acdbHostApplicationServices()->workingDatabase()->getBlockTable(pBlockTable, AcDb::kForRead) == Acad::eOk) {
		//	// Get the Model Space record and open it for write. This will be the owner of the new employee entity.
		//	AcDbBlockTableRecord *pSpaceRecord;
		//	if (pBlockTable->getAt(ACDB_MODEL_SPACE, pSpaceRecord, AcDb::kForWrite) == Acad::eOk) {
		//		AsdkEmployee *pEnt = new AsdkEmployee;
		//		pEnt->setID(id);
		//		pEnt->setCube(cubeNumber);
		//		pEnt->setFirstName(strFirstName);
		//		pEnt->setLastName(strLastName);
		//		pEnt->setCenter(pt);
		//		// Append pEnt to Model Space, then close it and the Model Space record.
		//		AcDbObjectId idObj;
		//		if (pSpaceRecord->appendAcDbEntity(idObj, pEnt) == Acad::eOk)
		//			pEnt->close();
		//		else
		//			delete pEnt;
		//		pSpaceRecord->close();
		//	}
		//	pBlockTable->close();
		//}

		// 20181104, Copy from -----------------
		int nColorIndex = 0; // 颜色索引值
		ads_real width = 0;  // 多段线的线宽
		int nIndex = 2;  // 当前输入点的次数
		//ads_point ptStart; // 起点
		//ads_point ptPrevious;// 前一个参考点
		//ads_point ptCurrent; //当前拾取的点
		AcGePoint3d ptStart; // 起点
		AcGePoint3d ptPrevious;// 前一个参考点
		AcGePoint3d ptCurrent; //当前拾取的点
		AcDbObjectId polyId; //多段线ID

		// 提示用户输入起点
		//if (RTNORM != acedGetPoint(NULL, _T("\n输入第一点:"), ptStart)) //For ads_point
		if (RTNORM != acedGetPoint(NULL, _T("\nInput First Point:"), asDblArray(ptStart)))
		{
			return;
		}

		//AcGePoint3d fc_retr_point_single(const AcGePoint3d &startpunkt)
		//{
		//	AcGePoint3d endpunkt;
		//	acedGetPoint(asDblArray(startpunkt), _T("\nPunkt: "), asDblArray(endpunkt));
		//	return endpunkt;
		//}

		// 输入第二点
		acedInitGet(NULL, _T("W C B F"));
		//int rc = acedGetPoint(asDblArray(ptStart), _T("\nNext Point [Width(W)/Color(C)/Close(B)]<Finish(F)>:"), asDblArray(ptCurrent));
		int rc = acedGetPoint(asDblArray(ptStart), _T("\nNext Point [Close(B)]<Finish(F)>:"), asDblArray(ptCurrent));
		while (RTNORM == rc || RTKWORD == rc)
		{
			if (RTKWORD == rc) // 如果用户输入了关键字
			{
				ACHAR keyWord[20];// 关键字
				//获取输入的关键字
				if (RTNORM != acedGetInput(keyWord))
				{
					return;
				}

				if (0 == _tcscmp(keyWord, _T("W")))
				{
					/*width = AsdkB3DPolyline::GetWidth();
					AcDbPolyline *pPoly = AsdkB3DPolyline::GetPolyLine(polyId);
					if (NULL == pPoly)
					{
						return;
					}
					pPoly->setConstantWidth(width);
					pPoly->close();*/
					acutPrintf(_T("\nNo need for Line Width Change."));
				}
				else if (0 == _tcscmp(keyWord, _T("C")))
				{
					/*nColorIndex = AsdkB3DPolyline::GetColorIndex();
					AcDbPolyline *pPoly = AsdkB3DPolyline::GetPolyLine(polyId);
					if (NULL == pPoly)
					{
						return;
					}
					pPoly->setColorIndex(nColorIndex);
					pPoly->close();*/
					acutPrintf(_T("\nNo need for Color Change."));
				}
				else if (0 == _tcscmp(keyWord, _T("B")))
				{
					if (nIndex < 3)
					{
						acutPrintf(_T("\nNot enough points for closing."));
						return;
					}

					AcDb3dPolyline *pPoly = AsdkB3DPolyline::GetB3DPolyLine(polyId);
					if (NULL == pPoly)
					{
						return;
					}
					pPoly->setClosed(Adesk::kTrue);
					pPoly->close();
					return;
				}
				else if (0 == _tcscmp(keyWord, _T("F")))
				{
					return;
				}
				else
				{
					acutPrintf(_T("\nNot Valid Input."));
				}
			}
			else if (RTNORM == rc)  // 用户输入了点
			{
				acutPrintf(_T("\nInput point is (%.2f, %.2f, %.2f)"), ptCurrent[X], ptCurrent[Y], ptCurrent[Z]);

				if (2 == nIndex)
				{
					// 创建多段线
					polyId = AsdkB3DPolyline::CreatePolyline(ptStart, ptCurrent); //, width, nColorIndex
					AsdkB3DPolyline::AddPoint(ptCurrent);
				}
				else if (nIndex > 2)
				{
					//修改多段线
					AsdkB3DPolyline::AddPolyline(polyId, ptCurrent); //, width, nColorIndex
				}

				++nIndex;
				acdbPointSet(asDblArray(ptCurrent), asDblArray(ptPrevious));//acdbPointSet宏： ads_point 变量值的复制
			}

			// 提示用户输入新的节点
			acedInitGet(NULL, _T("W C B F"));
			rc = acedGetPoint(asDblArray(ptPrevious), _T("\nNext Point [Close(B)]<Finish(F)>:"), asDblArray(ptCurrent));
		}
		// 20181104, Copy from -----------------
	}


	// Modal Command with pickfirst selection
	// ACED_ARXCOMMAND_ENTRY_AUTO(CKPipeLineApp, AsdkMyGroup, MyPickFirst, MyPickFirstLocal, ACRX_CMD_MODAL | ACRX_CMD_USEPICKSET)
	static void AsdkMyGroupMyPickFirst () {
		ads_name result ;
		int iRet =acedSSGet (ACRX_T("_I"), NULL, NULL, NULL, result) ;
		if ( iRet == RTNORM )
		{
			// There are selected entities
			// Put your command using pickfirst set code here
		}
		else
		{
			// There are no selected entities
			// Put your command code here
		}
	}

	// Application Session Command with localized name
	// ACED_ARXCOMMAND_ENTRY_AUTO(CKPipeLineApp, AsdkMyGroup, MySessionCmd, MySessionCmdLocal, ACRX_CMD_MODAL | ACRX_CMD_SESSION)
	static void AsdkMyGroupMySessionCmd () {
		// Put your command code here
	}

	// The ACED_ADSFUNCTION_ENTRY_AUTO / ACED_ADSCOMMAND_ENTRY_AUTO macros can be applied to any static member 
	// function of the CKPipeLineApp class.
	// The function may or may not take arguments and have to return RTNORM, RTERROR, RTCAN, RTFAIL, RTREJ to AutoCAD, but use
	// acedRetNil, acedRetT, acedRetVoid, acedRetInt, acedRetReal, acedRetStr, acedRetPoint, acedRetName, acedRetList, acedRetVal to return
	// a value to the Lisp interpreter.
	//
	// NOTE: ACED_ADSFUNCTION_ENTRY_AUTO / ACED_ADSCOMMAND_ENTRY_AUTO has overloads where you can provide resourceid.
	
	//- ACED_ADSFUNCTION_ENTRY_AUTO(classname, name, regFunc) - this example
	//- ACED_ADSSYMBOL_ENTRYBYID_AUTO(classname, name, nameId, regFunc) - only differs that it creates a localized name using a string in the resource file
	//- ACED_ADSCOMMAND_ENTRY_AUTO(classname, name, regFunc) - a Lisp command (prefix C:)
	//- ACED_ADSCOMMAND_ENTRYBYID_AUTO(classname, name, nameId, regFunc) - only differs that it creates a localized name using a string in the resource file

	// Lisp Function is similar to ARX Command but it creates a lisp 
	// callable function. Many return types are supported not just string
	// or integer.
	// ACED_ADSFUNCTION_ENTRY_AUTO(CKPipeLineApp, MyLispFunction, false)
	static int ads_MyLispFunction () {
		//struct resbuf *args =acedGetArgs () ;
		
		// Put your command code here

		//acutRelRb (args) ;
		
		// Return a value to the AutoCAD Lisp Interpreter
		// acedRetNil, acedRetT, acedRetVoid, acedRetInt, acedRetReal, acedRetStr, acedRetPoint, acedRetName, acedRetList, acedRetVal

		return (RTNORM) ;
	}
	
} ;

//-----------------------------------------------------------------------------
IMPLEMENT_ARX_ENTRYPOINT(CKPipeLineApp)

ACED_ARXCOMMAND_ENTRY_AUTO(CKPipeLineApp, AsdkKPipeLine, _B3DPOLY, B3DPOLY, ACRX_CMD_TRANSPARENT, NULL)

//ACED_ARXCOMMAND_ENTRY_AUTO(CKPipeLineApp, AsdkMyGroup, MyCommand, MyCommandLocal, ACRX_CMD_MODAL, NULL)
//ACED_ARXCOMMAND_ENTRY_AUTO(CKPipeLineApp, AsdkMyGroup, MyPickFirst, MyPickFirstLocal, ACRX_CMD_MODAL | ACRX_CMD_USEPICKSET, NULL)
//ACED_ARXCOMMAND_ENTRY_AUTO(CKPipeLineApp, AsdkMyGroup, MySessionCmd, MySessionCmdLocal, ACRX_CMD_MODAL | ACRX_CMD_SESSION, NULL)
//ACED_ADSSYMBOL_ENTRY_AUTO(CKPipeLineApp, MyLispFunction, false)

