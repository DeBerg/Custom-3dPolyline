﻿// (C) Copyright 2002-2007 by Autodesk, Inc. 
//
// Permission to use, copy, modify, and distribute this software in
// object code form for any purpose and without fee is hereby granted, 
// provided that the above copyright notice appears in all copies and 
// that both that copyright notice and the limited warranty and
// restricted rights notice below appear in all supporting 
// documentation.
//
// AUTODESK PROVIDES THIS PROGRAM "AS IS" AND WITH ALL FAULTS. 
// AUTODESK SPECIFICALLY DISCLAIMS ANY IMPLIED WARRANTY OF
// MERCHANTABILITY OR FITNESS FOR A PARTICULAR USE.  AUTODESK, INC. 
// DOES NOT WARRANT THAT THE OPERATION OF THE PROGRAM WILL BE
// UNINTERRUPTED OR ERROR FREE.
//
// Use, duplication, or disclosure by the U.S. Government is subject to 
// restrictions set forth in FAR 52.227-19 (Commercial Computer
// Software - Restricted Rights) and DFAR 252.227-7013(c)(1)(ii)
// (Rights in Technical Data and Computer Software), as applicable.
//

//-----------------------------------------------------------------------------
//----- AsdkB3Dpolyline.cpp : Implementation of AsdkB3Dpolyline
//-----------------------------------------------------------------------------
#include "StdAfx.h"
#include "tchar.h"
#include "AsdkB3DPolyline.h"


//-----------------------------------------------------------------------------
Adesk::UInt32 AsdkB3DPolyline::kCurrentVersionNumber =1 ;

//-----------------------------------------------------------------------------
ACRX_DXF_DEFINE_MEMBERS (
	AsdkB3DPolyline, AcDbCurve,
	AcDb::kDHL_CURRENT, AcDb::kMReleaseCurrent, 
	AcDbProxyEntity::kNoOperation, ASDKB3DPOLYLINE,
ASDKB3DPOLYAPP
|Product Desc:     A description for your object
|Company:          Your company name
|WEB Address:      Your company WEB site address
)

//-----------------------------------------------------------------------------
//AsdkB3DPolyline::AsdkB3DPolyline () : AcDb3dPolyline() {
AsdkB3DPolyline::AsdkB3DPolyline() : AcDbCurve() {
	acutPrintf(_T("\nNow in AsdkB3Dpolyline Constructor."));
}

AsdkB3DPolyline::~AsdkB3DPolyline () {
}

//-----------------------------------------------------------------------------
//----- AcDbObject protocols
//- Dwg Filing protocol
Acad::ErrorStatus AsdkB3DPolyline::dwgOutFields (AcDbDwgFiler *pFiler) const {
	assertReadEnabled () ;
	//----- Save parent class information first.
	Acad::ErrorStatus es =AcDbCurve::dwgOutFields (pFiler) ;
	if ( es != Acad::eOk )
		return (es) ;
	//----- Object version number needs to be saved first
	if ( (es =pFiler->writeUInt32 (AsdkB3DPolyline::kCurrentVersionNumber)) != Acad::eOk )
		return (es) ;
	//----- Output params
	//.....

	return (pFiler->filerStatus ()) ;
}

Acad::ErrorStatus AsdkB3DPolyline::dwgInFields (AcDbDwgFiler *pFiler) {
	assertWriteEnabled () ;
	//----- Read parent class information first.
	Acad::ErrorStatus es =AcDbCurve::dwgInFields (pFiler) ;
	if ( es != Acad::eOk )
		return (es) ;
	//----- Object version number needs to be read first
	Adesk::UInt32 version =0 ;
	if ( (es =pFiler->readUInt32 (&version)) != Acad::eOk )
		return (es) ;
	if ( version > AsdkB3DPolyline::kCurrentVersionNumber )
		return (Acad::eMakeMeProxy) ;
	//- Uncomment the 2 following lines if your current object implementation cannot
	//- support previous version of that object.
	//if ( version < AsdkB3Dpolyline::kCurrentVersionNumber )
	//	return (Acad::eMakeMeProxy) ;
	//----- Read params
	//.....

	return (pFiler->filerStatus ()) ;
}

//-----------------------------------------------------------------------------
//----- AcDbEntity protocols
Adesk::Boolean AsdkB3DPolyline::subWorldDraw (AcGiWorldDraw *mode) {
	assertReadEnabled () ;
	return (AcDbCurve::subWorldDraw (mode)) ;
}


Adesk::UInt32 AsdkB3DPolyline::subSetAttributes (AcGiDrawableTraits *traits) {
	assertReadEnabled () ;
	return (AcDbCurve::subSetAttributes (traits)) ;
}

	//- Osnap points protocol
Acad::ErrorStatus AsdkB3DPolyline::subGetOsnapPoints (
	AcDb::OsnapMode osnapMode,
	Adesk::GsMarker gsSelectionMark,
	const AcGePoint3d &pickPoint,
	const AcGePoint3d &lastPoint,
	const AcGeMatrix3d &viewXform,
	AcGePoint3dArray &snapPoints,
	AcDbIntArray &geomIds) const
{
	assertReadEnabled () ;
	return (AcDbCurve::subGetOsnapPoints (osnapMode, gsSelectionMark, pickPoint, lastPoint, viewXform, snapPoints, geomIds)) ;
}

Acad::ErrorStatus AsdkB3DPolyline::subGetOsnapPoints (
	AcDb::OsnapMode osnapMode,
	Adesk::GsMarker gsSelectionMark,
	const AcGePoint3d &pickPoint,
	const AcGePoint3d &lastPoint,
	const AcGeMatrix3d &viewXform,
	AcGePoint3dArray &snapPoints,
	AcDbIntArray &geomIds,
	const AcGeMatrix3d &insertionMat) const
{
	assertReadEnabled () ;
	return (AcDbCurve::subGetOsnapPoints (osnapMode, gsSelectionMark, pickPoint, lastPoint, viewXform, snapPoints, geomIds, insertionMat)) ;
}

//- Grip points protocol
Acad::ErrorStatus AsdkB3DPolyline::subGetGripPoints (
	AcGePoint3dArray &gripPoints, AcDbIntArray &osnapModes, AcDbIntArray &geomIds
) const {
	assertReadEnabled () ;
	//----- This method is never called unless you return eNotImplemented 
	//----- from the new getGripPoints() method below (which is the default implementation)

	return (AcDbCurve::subGetGripPoints (gripPoints, osnapModes, geomIds)) ;
}

Acad::ErrorStatus AsdkB3DPolyline::subMoveGripPointsAt (const AcDbIntArray &indices, const AcGeVector3d &offset) {
	assertWriteEnabled () ;
	//----- This method is never called unless you return eNotImplemented 
	//----- from the new moveGripPointsAt() method below (which is the default implementation)

	return (AcDbCurve::subMoveGripPointsAt (indices, offset)) ;
}

Acad::ErrorStatus AsdkB3DPolyline::subGetGripPoints (
	AcDbGripDataPtrArray &grips, const double curViewUnitSize, const int gripSize, 
	const AcGeVector3d &curViewDir, const int bitflags
) const {
	assertReadEnabled () ;

	//----- If you return eNotImplemented here, that will force AutoCAD to call
	//----- the older getGripPoints() implementation. The call below may return
	//----- eNotImplemented depending of your base class.
	return (AcDbCurve::subGetGripPoints (grips, curViewUnitSize, gripSize, curViewDir, bitflags)) ;
}

Acad::ErrorStatus AsdkB3DPolyline::subMoveGripPointsAt (
	const AcDbVoidPtrArray &gripAppData, const AcGeVector3d &offset,
	const int bitflags
) {
	assertWriteEnabled () ;

	//----- If you return eNotImplemented here, that will force AutoCAD to call
	//----- the older getGripPoints() implementation. The call below may return
	//----- eNotImplemented depending of your base class.
	return (AcDbCurve::subMoveGripPointsAt (gripAppData, offset, bitflags)) ;
}

//-----------------------------------------------------------------------------
//----- AcDbCurve protocols
//- Curve property tests.
Adesk::Boolean AsdkB3DPolyline::isClosed () const {
	assertReadEnabled () ;
	return (AcDbCurve::isClosed ()) ;
}

Adesk::Boolean AsdkB3DPolyline::isPeriodic () const {
	assertReadEnabled () ;
	return (AcDbCurve::isPeriodic ()) ;
}
      
//- Get planar and start/end geometric properties.
Acad::ErrorStatus AsdkB3DPolyline::getStartParam (double &param) const {
	assertReadEnabled () ;
	return (AcDbCurve::getStartParam (param)) ;
}

Acad::ErrorStatus AsdkB3DPolyline::getEndParam (double &param) const {
	assertReadEnabled () ;
	return (AcDbCurve::getEndParam (param)) ;
}

Acad::ErrorStatus AsdkB3DPolyline::getStartPoint (AcGePoint3d &point) const {
	assertReadEnabled () ;
	return (AcDbCurve::getStartPoint (point)) ;
}

Acad::ErrorStatus AsdkB3DPolyline::getEndPoint (AcGePoint3d &point) const {
	assertReadEnabled () ;
	return (AcDbCurve::getEndPoint (point)) ;
}

//- Conversions to/from parametric/world/distance.
Acad::ErrorStatus AsdkB3DPolyline::getPointAtParam (double param, AcGePoint3d &point) const {
	assertReadEnabled () ;
	return (AcDbCurve::getPointAtParam (param, point)) ;
}

Acad::ErrorStatus AsdkB3DPolyline::getParamAtPoint (const AcGePoint3d &point, double &param) const {
	assertReadEnabled () ;
	return (AcDbCurve::getParamAtPoint (point, param)) ;
}

Acad::ErrorStatus AsdkB3DPolyline::getDistAtParam (double param, double &dist) const {
	assertReadEnabled () ;
	return (AcDbCurve::getDistAtParam (param, dist)) ;
}

Acad::ErrorStatus AsdkB3DPolyline::getParamAtDist (double dist, double &param) const {
	assertReadEnabled () ;
	return (AcDbCurve::getParamAtDist (dist, param)) ;
}

Acad::ErrorStatus AsdkB3DPolyline::getDistAtPoint (const AcGePoint3d &point , double &dist) const {
	assertReadEnabled () ;
	return (AcDbCurve::getDistAtPoint (point, dist)) ;
}

Acad::ErrorStatus AsdkB3DPolyline::getPointAtDist (double dist, AcGePoint3d &point) const {
	assertReadEnabled () ;
	return (AcDbCurve::getPointAtDist (dist, point)) ;
}

//- Derivative information.
Acad::ErrorStatus AsdkB3DPolyline::getFirstDeriv (double param, AcGeVector3d &firstDeriv) const {
	assertReadEnabled () ;
	return (AcDbCurve::getFirstDeriv (param, firstDeriv)) ;
}

Acad::ErrorStatus AsdkB3DPolyline::getFirstDeriv  (const AcGePoint3d &point, AcGeVector3d &firstDeriv) const {
	assertReadEnabled () ;
	return (AcDbCurve::getFirstDeriv (point, firstDeriv)) ;
}

Acad::ErrorStatus AsdkB3DPolyline::getSecondDeriv (double param, AcGeVector3d &secDeriv) const {
	assertReadEnabled () ;
	return (AcDbCurve::getSecondDeriv (param, secDeriv)) ;
}

Acad::ErrorStatus AsdkB3DPolyline::getSecondDeriv (const AcGePoint3d &point, AcGeVector3d &secDeriv) const {
	assertReadEnabled () ;
	return (AcDbCurve::getSecondDeriv (point, secDeriv)) ;
}

//- Closest point on curve.
Acad::ErrorStatus AsdkB3DPolyline::getClosestPointTo (const AcGePoint3d &givenPnt, AcGePoint3d &pointOnCurve, Adesk::Boolean extend /*=Adesk::kFalse*/) const {
	assertReadEnabled () ;
	return (AcDbCurve::getClosestPointTo (givenPnt, pointOnCurve, extend)) ;
}

Acad::ErrorStatus AsdkB3DPolyline::getClosestPointTo (const AcGePoint3d &givenPnt, const AcGeVector3d &direction, AcGePoint3d &pointOnCurve, Adesk::Boolean extend /*=Adesk::kFalse*/) const {
	assertReadEnabled () ;
	return (AcDbCurve::getClosestPointTo (givenPnt, direction, pointOnCurve, extend)) ;
}

//- Get a projected copy of the curve.
Acad::ErrorStatus AsdkB3DPolyline::getOrthoProjectedCurve (const AcGePlane &plane, AcDbCurve *&projCrv) const {
	assertReadEnabled () ;
	return (AcDbCurve::getOrthoProjectedCurve (plane, projCrv)) ;
}

Acad::ErrorStatus AsdkB3DPolyline::getProjectedCurve (const AcGePlane &plane, const AcGeVector3d &projDir, AcDbCurve *&projCrv) const {
	assertReadEnabled () ;
	return (AcDbCurve::getProjectedCurve (plane, projDir, projCrv)) ;
}

//- Get offset, spline and split copies of the curve.
Acad::ErrorStatus AsdkB3DPolyline::getOffsetCurves (double offsetDist, AcDbVoidPtrArray &offsetCurves) const {
	assertReadEnabled () ;
	return (AcDbCurve::getOffsetCurves (offsetDist, offsetCurves)) ;
}

Acad::ErrorStatus AsdkB3DPolyline::getOffsetCurvesGivenPlaneNormal (const AcGeVector3d &normal, double offsetDist, AcDbVoidPtrArray &offsetCurves) const {
	assertReadEnabled () ;
	return (AcDbCurve::getOffsetCurvesGivenPlaneNormal (normal, offsetDist, offsetCurves)) ;
}

Acad::ErrorStatus AsdkB3DPolyline::getSpline (AcDbSpline *&spline) const {
	assertReadEnabled () ;
	return (AcDbCurve::getSpline (spline)) ;
}

Acad::ErrorStatus AsdkB3DPolyline::getSplitCurves (const AcGeDoubleArray &params, AcDbVoidPtrArray &curveSegments) const {
	assertReadEnabled () ;
	return (AcDbCurve::getSplitCurves (params, curveSegments)) ;
}

Acad::ErrorStatus AsdkB3DPolyline::getSplitCurves (const AcGePoint3dArray &points, AcDbVoidPtrArray &curveSegments) const {
	assertReadEnabled () ;
	return (AcDbCurve::getSplitCurves (points, curveSegments)) ;
}

//- Extend the curve.
Acad::ErrorStatus AsdkB3DPolyline::extend (double newParam) {
	assertReadEnabled () ;
	return (AcDbCurve::extend (newParam)) ;
}

Acad::ErrorStatus AsdkB3DPolyline::extend (Adesk::Boolean extendStart, const AcGePoint3d &toPoint) {
	assertReadEnabled () ;
	return (AcDbCurve::extend (extendStart, toPoint)) ;
}

//- Area calculation.
Acad::ErrorStatus AsdkB3DPolyline::getArea (double &area) const {
	assertReadEnabled () ;
	return (AcDbCurve::getArea (area)) ;
}


// 20181104, Copy from -----------------
#include <atlstr.h>

bool AsdkB3DPolyline::PostToModelSpace(AcDbEntity* pEnt, AcDbObjectId &entId)
{
	if (NULL == pEnt)
	{
		return false;
	}
	// 获得指向块表的指针
	AcDbBlockTable *pBlockTable = NULL;
	//workingDatabase()能够获得一个指向当前活动的图形数据库的指针，
	acdbHostApplicationServices()->workingDatabase()->getBlockTable(pBlockTable, AcDb::kForRead);
	if (NULL == pBlockTable)
	{
		return false;
	}

	// 获得指向特定的块表记录（模型空间）的指针
	AcDbBlockTableRecord *pBlockTableRecord = NULL;
	pBlockTable->getAt(ACDB_MODEL_SPACE, pBlockTableRecord, AcDb::kForWrite);
	if (NULL == pBlockTableRecord)
	{
		return false;
	}

	// 将AcDbLine类的对象添加到块表记录中
	pBlockTableRecord->appendAcDbEntity(entId, pEnt);

	// 关闭图形数据库的各种对象
	pBlockTable->close();
	pBlockTableRecord->close();
	pEnt->close();

	return true;
}

bool AsdkB3DPolyline::PostToModelSpace(AcDbEntity* pEnt)
{
	if (NULL == pEnt)
	{
		return false;
	}
	// 获得指向块表的指针
	AcDbBlockTable *pBlockTable = NULL;
	//workingDatabase()能够获得一个指向当前活动的图形数据库的指针，
	acdbHostApplicationServices()->workingDatabase()->getBlockTable(pBlockTable, AcDb::kForRead);
	if (NULL == pBlockTable)
	{
		return false;
	}

	// 获得指向特定的块表记录（模型空间）的指针
	AcDbBlockTableRecord *pBlockTableRecord = NULL;
	pBlockTable->getAt(ACDB_MODEL_SPACE, pBlockTableRecord, AcDb::kForWrite);
	if (NULL == pBlockTableRecord)
	{
		return false;
	}

	// 将AcDbLine类的对象添加到块表记录中
	pBlockTableRecord->appendAcDbEntity(pEnt);

	// 关闭图形数据库的各种对象
	pBlockTable->close();
	pBlockTableRecord->close();
	pEnt->close();

	return true;
}

AcDb3dPolyline * AsdkB3DPolyline::GetB3DPolyLine(AcDbObjectId polyId)
{
	AcDb3dPolyline * pPoly(NULL);
	if (Acad::eOk != acdbOpenObject(pPoly, polyId, AcDb::kForWrite))
	{
		return NULL;
	}
	return pPoly;
}

//void genPtArray(AcGePoint3dArray &pts, AcGePoint3d &pt1, AcGePoint3d &pt2)
//{
//	pts.removeAll;
//	pts.append(pt1);
//	pts.append(pt2);
//
//}

AcDbObjectId AsdkB3DPolyline::CreatePolyline(AcGePoint3d ptStart, AcGePoint3d ptEnd)//, double width, int nColorIndex
{
	/*AcDbPolyline *pPoly = new AcDbPolyline;

	AcGePoint2d ptInsert = asPnt2d(ptStart);
	AcGePoint2d ptInsert1 = asPnt2d(ptCurrent);

	pPoly->addVertexAt(0, ptInsert, nColorIndex, width, width);
	pPoly->addVertexAt(1, ptInsert1, nColorIndex, width, width);

	AcDbObjectId polyId;
	if (!AsdkB3DPolyline::PostToModelSpace(pPoly, polyId))
	{
		acutPrintf(_T("\nAdd to Graphic Database Failed."));
		return NULL;
	}
	return polyId;*/

	AcGePoint3dArray ptArr; 
	AsdkB3DPolyline::genPtArray(ptArr, ptStart, ptEnd);
	/*ptArr->append(ptStart);
	ptArr->append(ptCurrent);*/


	AcDb3dPolyline *pPoly = new AcDb3dPolyline(AcDb::k3dSimplePoly, ptArr,Adesk::kFalse);
	

	AcDbObjectId polyId;
	if (!AsdkB3DPolyline::PostToModelSpace(pPoly, polyId))
	{
		acutPrintf(_T("\nAdd to Graphic Database Failed."));
		return NULL;
	}
	return polyId;
}

//AcDbObjectId AsdkB3DPolyline::CreatePolyline(AsdkB3DVertex vtStart, AsdkB3DVertex vtEnd)
//{
//	AcGePoint3dArray ptArr;
//	AsdkB3DPolyline::genPtArray(ptArr, vtStart.position, vtEnd.position);
//
//	AcDb3dPolyline *pPoly = new AcDb3dPolyline(AcDb::k3dSimplePoly, ptArr, Adesk::kFalse);
//	
//	AcDbObjectId polyId;
//	if (!AsdkB3DPolyline::PostToModelSpace(pPoly, polyId))
//	{
//		acutPrintf(_T("\nAdd to Graphic Database Failed."));
//		return NULL;
//	}
//
//
//	return polyId;
//}

void AsdkB3DPolyline::AddPolyline(AcDbObjectId polyId, AcGePoint3d ptCurrent) //, double width, int nColorIndex
{
	AcDb3dPolyline *pPoly = GetB3DPolyLine(polyId);
	if (NULL == pPoly)
	{
		return;
	}

	acutPrintf(_T("\nTest 1 OK."));

	AcDb3dPolylineVertex *pVert = new AcDb3dPolylineVertex(ptCurrent);
	//AsdkB3DVertex *pVert = new AsdkB3DVertex(ptCurrent,888);

	//AcGePoint2d ptInsert = asPnt2d(ptCurrent);
	//pPoly->addVertexAt(nIndex - 1, ptInsert, nColorIndex, width, width);
	if (pPoly->appendVertex(pVert) != Acad::eOk) {
		acutPrintf(_T("\nTest 2 NOT OK."));
		pPoly->close();
		return;
	}
	acutPrintf(_T("\nTest 2 OK."));
	//pPoly->appendVertex(pVert);
	pPoly->close();
	pVert->close();
}

//void AsdkB3DPolyline::AddPolyline(AcDbObjectId polyId, AsdkB3DVertex vtCurrent) // Append B3DVertex
//{
//	AcDb3dPolyline *pPoly = GetB3DPolyLine(polyId);
//	if (NULL == pPoly)
//	{
//		return;
//	}
//
//	acutPrintf(_T("\nTest 11 OK."));
//	AcGePoint3d nPt3d = vtCurrent.position;
//
//	AcDb3dPolylineVertex *pVert = new AcDb3dPolylineVertex(nPt3d);
//	//AsdkB3DVertex *pVert = new AsdkB3DVertex(ptCurrent,888);
//
//	//AcGePoint2d ptInsert = asPnt2d(ptCurrent);
//	//pPoly->addVertexAt(nIndex - 1, ptInsert, nColorIndex, width, width);
//	if (pPoly->appendVertex(pVert) != Acad::eOk) {
//		acutPrintf(_T("\nTest 12 NOT OK."));
//		pPoly->close();
//		return;
//	}
//	acutPrintf(_T("\nTest 12 OK."));
//	//pPoly->appendVertex(pVert);
//	pPoly->close();
//	pVert->close();
//}

void AsdkB3DPolyline::AddPoint(AcGePoint3d ptCurrent) {
	AsdkB3DVertex *pB3Ver = new AsdkB3DVertex(ptCurrent,999);


	if (!AsdkB3DPolyline::PostToModelSpace(pB3Ver))
	{
		acutPrintf(_T("\nAdd B3DVertex to Database Failed."));
	}
}

//ads_real AsdkB3DPolyline::GetWidth()
//{
//	ads_real width = 0;
//	if (RTNORM == acedGetReal(_T("\n输入线宽:"), &width))
//	{
//		return width;
//	}
//	else
//	{
//		return 0;
//	}
//}
//
//int AsdkB3DPolyline::GetColorIndex()
//{
//	int nColorIndex = 0;
//	if (RTNORM != acedGetInt(_T("\n输入颜色索引值(0～256):"), &nColorIndex))
//	{
//		return 0;
//	}
//
//	// 处理颜色索引值无效的情况
//	while (nColorIndex < 0 || nColorIndex > 256)
//	{
//		acedPrompt(_T("\n输入了无效的颜色索引."));
//		if (RTNORM != acedGetInt(_T("\n输入颜色索引值(0～256):"), &nColorIndex))
//		{
//			return 0;
//		}
//	}
//
//	return nColorIndex;
//}

void AsdkB3DPolyline::genPtArray(AcGePoint3dArray &pts, AcGePoint3d pt1, AcGePoint3d pt2)
{
	//pts.removeAll;
	pts.append(pt1);
	pts.append(pt2);
	
}

//static AcGePoint3d toPt3d(AsdkB3DVertex vert)
//{
//	return vert.position;
//}

//void getPoints(AcGePoint3dArray &pts)
//{
//	const double r = 5.0;
//	const double pi2 = 2 * 3.1415926535897932384626433832795;
//	const int step1 = 60, step2 = 60;
//	const double d1 = pi2 / step1, d2 = pi2 / step2;
//
//	int n1, n2;
//	double phi1, phi2;
//	double lxy;
//	AcGePoint3d pt;
//	for (n2 = 0; n2 < step2; ++n2)
//	{
//		phi2 = n2 * d2;
//		pt.z = r * sin(phi2);
//		lxy = r * cos(phi2);
//		for (n1 = 0; n1 < step1; ++n1)
//		{
//			phi1 = n1 * d1;
//			pt.x = lxy * sin(phi1);
//			pt.y = lxy * cos(phi1);
//			pts.append(pt);
//		}
//	}
//}